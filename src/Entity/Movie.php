<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MovieRepository")
 * @ORM\Table(
 *     name="movie",
 *     uniqueConstraints={@UniqueConstraint(name="reference_unique",columns={"omdb_id"})},
 *     )
 */
class Movie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $omdbId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $poster;

    /**
     * Movie constructor.
     *
     * @param $omdbId
     * @param $title
     * @param $poster
     */
    public function __construct($omdbId, $title, $poster)
    {
        $this->omdbId = $omdbId;
        $this->title  = $title;
        $this->poster = $poster;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOmdbId(): ?string
    {
        return $this->omdbId;
    }

    public function setOmdbId(string $omdbId): self
    {
        $this->omdbId = $omdbId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPoster(): ?string
    {
        return $this->poster;
    }

    public function setPoster(string $poster): self
    {
        $this->poster = $poster;

        return $this;
    }
}
