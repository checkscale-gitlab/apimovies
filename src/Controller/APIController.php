<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\Users;
use App\Form\UsersType;
use App\Manager\MovieManager;
use App\Manager\VoteManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class APIController extends AbstractController
{
    /**
     * @Route("/new/user", name="new_user")
     * @Method("POST")
     */
    public function newUser(
        Request $request,
        EntityManagerInterface $entityManager
    ) {
        $body = $request->getContent();
        $data = json_decode($body, true);

        // Use FormType to create the Users
        $user = new Users();
        $form = $this->createForm(UsersType::class, $user);
        $form->submit($data);

        if (!$form->isValid()) {
            throw new InvalidArgumentException(null, 1);
        }

        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse(
            [
                'message' => $user->getNickname() . ' was created.',
                'userId'  => $user->getId(),
                'code'    => 0,
            ]
            , Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/vote/movie", name="submit_vote")
     * @Method("POST")
     */
    public function submitVote(
        Request $request,
        EntityManagerInterface $entityManager,
        MovieManager $movieManager,
        VoteManager $voteManager
    ) {
        $body = $request->getContent();
        $data = json_decode($body, true);

        if (!array_key_exists('userId', $data) || !array_key_exists('vote', $data) || !array_key_exists('omdbId', $data['vote'])) {
            throw new InvalidArgumentException(null, 2);
        }

        $user = $entityManager->getRepository(Users::class)->find($data['userId']);

        if (!$user instanceof Users) {
            throw new InvalidArgumentException('User with id ' . $data['userId'] . ' was not found', 3);
        }

        # Movie selected
        $choice = $data['vote'];
        $movie  = $movieManager->getOrCreateMovie($choice['omdbId'], $choice['title'], $choice['poster']);

        # Vote
        if ($voteManager->voteMovie($user, $movie)) {
            $message = $user->getNickname() . ", your vote for " . $movie->getTitle() . " is now registered";
            $code    = 0;
            $status  = Response::HTTP_CREATED;
        } else {
            $message = $user->getNickname() . " you already voted 3 times";
            $code    = 4;
            $status  = Response::HTTP_NOT_MODIFIED;
        }

        return new JsonResponse(
            [
                "message" => $message,
                "code"    => $code,
            ]
            , $status
        );
    }

    /**
     * @Route("/unvote/movie", name="submit_unvote")
     * @Method("DELETE")
     */
    public function submitUnvote(Request $request, EntityManagerInterface $entityManager, MovieManager $movieManager)
    {
        $body = $request->getContent();
        $data = json_decode($body, true);

        if (!array_key_exists('userId', $data) || !array_key_exists('vote', $data) || !array_key_exists('omdbId', $data['vote'])) {
            throw new InvalidArgumentException(null, 2);
        }

        $user = $entityManager->getRepository(Users::class)->find($data['userId']);

        if (!$user instanceof Users) {
            throw new EntityNotFoundException('User with id ' . $data['userId'] . ' was not found', 3);
        }

        # Movie selected
        $choice = $data['vote'];
        $movie  = $entityManager->getRepository(Movie::class)->findOneBy(['omdbId' => $choice['omdbId']]);

        if (!$movie instanceof Movie) {
            throw new EntityNotFoundException('This Movie does not exists', 5);
        }

        $user->removeVote($movie);

        $entityManager->persist($user);
        $entityManager->flush();

        return new JsonResponse(
            [
                "message" => $user->getNickname() . ", your vote for " . $movie->getTitle() . " is removed",
                "code"    => 0,
            ]
            , Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/see/selected/{userId}", name="see_selection")
     * @Method("GET")
     */
    public function seeSelection($userId, Request $request, EntityManagerInterface $entityManager, MovieManager $movieManager)
    {
        $user = $entityManager->getRepository(Users::class)->find($userId);

        if (!$user instanceof Users) {
            throw new InvalidArgumentException('User with id ' . $userId . ' was not found', 3);
        }

        return new JsonResponse(
            [
                "choices" => $movieManager->formatExit($user->getVotes()->toArray()),
                "code"    => 0,
            ]
            , Response::HTTP_CREATED
        );
    }

    /**
     * @Route("/see/participants", name="see_participants")
     * @Method("GET")
     */
    public function seeParticipants(EntityManagerInterface $entityManager, VoteManager $voteManager)
    {
        $participants = $voteManager->findAllVotingUsers();

        return new JsonResponse(

            [
                "participants" => $participants,
                "code"         => 0,
            ]
            , Response::HTTP_OK
        );
    }

    /**
     * @Route("/see/results", name="see_results")
     * @Method("GET")
     */
    public function seeResults(EntityManagerInterface $entityManager, VoteManager $voteManager)
    {
        $movie = $entityManager->getRepository(Movie::class)->findBest();

        return new JsonResponse(
            [
                "first" => $movie,
                "code"  => 0,
            ]
            , Response::HTTP_OK
        );
    }
}
