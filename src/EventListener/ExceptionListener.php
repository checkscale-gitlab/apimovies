<?php

namespace App\EventListener;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Form\Exception\InvalidArgumentException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * Class ExceptionListener
 * @package App\EventListener
 */
class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();

        // Default values
        $code    = $exception->getCode();
        $message = "Woops, an error occurred while processing this request.";
        $status  = Response::HTTP_INTERNAL_SERVER_ERROR;

        $response = new Response();

        if ($exception instanceof HttpExceptionInterface) {
            $status = $exception->getStatusCode();
            $response->headers->replace($exception->getHeaders());

            if ($status == Response::HTTP_NOT_FOUND) {
                $message = ["problem" => "Route does not exists", "solution" => "You must hire Pec to solve it!"];
                $code    = 3 * 3 * 74;
            } else {
                $response->setStatusCode($status);
                $code = 6;
            }
        } elseif ($exception instanceof InvalidArgumentException) {
            $message = $exception->getMessage() ?: "The input format does not match the required one for this API";
            $status  = Response::HTTP_BAD_REQUEST;
        } elseif ($exception instanceof ValidatorException) {
            // @TODO
        } elseif ($exception instanceof EntityNotFoundException) {
            $message = $exception->getMessage();
            $status  = Response::HTTP_NO_CONTENT;
        }

        // Sends the modified response object to the event
        $response->setContent(json_encode(["message" => $message, "code" => $code]));
        $response->setStatusCode($status);
        $response->headers->set('Content-Type', 'application/json');

        $event->setResponse($response);
    }
}
