<?php

namespace App\Manager;

use App\Entity\Movie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class MovieManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ThirdPartyManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $omdbId
     * @param string $title
     * @param string $poster
     *
     * @return Movie
     */
    public function getOrCreateMovie(string $omdbId, string $title, string $poster): Movie
    {
        $search = $this->entityManager->getRepository(Movie::class)->findOneBy(['omdbId' => $omdbId]);

        if ($search instanceof Movie) {
            $movie = $search;
        } else {
            $movie = new Movie($omdbId, $title, $poster);
            $this->entityManager->persist($movie);
            $this->entityManager->flush();
        }

        return $movie;
    }

    /**
     * @param array $movies
     *
     * @return array
     */
    public function formatExit(array $movies): array
    {
        $result = [];

        foreach ($movies as $movie) {
            $result[] = [
                'title'  => $movie->getTitle(),
                'omdbId' => $movie->getOmdbId(),
                'poster' => $movie->getPoster(),
            ];
        }

        return $result;
    }
}
